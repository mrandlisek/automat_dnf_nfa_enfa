window.Automat = {
    vrcholy: [],
    aktivneVrcholy: [0],
    aktivneStavy: [],

    strom: function (stav) {
        this.vrcholy.push({
            'otec': -1,
            'stav': stav,
            'znak': '$'
        });
        this.aktivneStavy.push(stav);
    },
    vypisVrcholy: function(){
        console.log('pocet vrcholov v strome: ', this.vrcholy.length);
    },
    resetuj: function(){
        this.aktivneStavy = [];
        this.aktivneVrcholy = [];
    },
    pridajVrchol: function(otec, stav, znak){
        if (!this.aktivneStavy.includes(stav)){
            this.vrcholy.push({
                'otec': otec,
                'stav': stav,
                'znak': znak
            });
            this.aktivneVrcholy.push(this.vrcholy.length-1);
            this.aktivneStavy.push(stav);
        }
    },
    /////////////////////////////////////////////////////////////////////////////////////////
    slovo: '', //bbbba
    stavy: [], //'1', '2', '3', '4'
    koncoveStavy: [], //'2', '3'
    startovaciStav: '', //'1'
    prechody: {
        // '1': {'a': ['2'], 'b': ['1']},  // b*a + b*aa
        // '2': {'a': ['3'], 'b': ['4']},
        // '3': {'a': ['4'], 'b': ['4']},
        // '4': {'a': ['4'], 'b': ['4']},
        // '1': {'a': ['1'], 'b': ['2', '3'], '$': ['3']}, // a*bb*a*
        // '2': {'a': ['3'], 'b': ['2']},
        // '3': {'a': ['3'], 'b': ['4']},
        // '4': {'a': ['4'], 'b': ['4']},
    },
    /////////////////////////////////////////////////////////////////////////////////////////
    iterator: 0,

    init: function () {
        this.strom(this.startovaciStav);
        for (this.iterator = 0; this.iterator < this.slovo.length; this.iterator++) {
            this.getPrechody();
        }
        var nasloCestu = true;
        for ( var i = 0; i < this.aktivneVrcholy.length; i++ ) {
            var jeNaKonci = false;
            for ( var j = 0; j < this.koncoveStavy.length; j++ ) {
                if (this.vrcholy[this.aktivneVrcholy[i]]['stav'] == this.koncoveStavy[j]){
                    nasloCestu = false;
                    jeNaKonci = true;
                }
            }
            if (jeNaKonci){
                return ['možné riešenie je: ', this.postavCestu(this.aktivneVrcholy[i])];
            }
            else {
                return ['Automat skončí v neakceptujúcom stave: ', this.postavCestu(this.aktivneVrcholy[i])]
            }
        }

        if (nasloCestu) {
            return ['V automate sa nepodarilo nájsť cestu', "Tento automat nemá riešenie."];
        }
        this.vypisVrcholy()
    },
    getPrechody: function () {
        var prechody = [];
        var idPrechodov = [];
        for (var i = 0; i < this.aktivneVrcholy.length; i++) {
            if (this.prechody[this.vrcholy[this.aktivneVrcholy[i]]['stav']].hasOwnProperty('$')) {
                for (var j = 0; j < this.prechody[this.vrcholy[this.aktivneVrcholy[i]]['stav']]['$'].length; j++){
                    this.pridajVrchol(this.aktivneVrcholy[i], this.prechody[this.vrcholy[this.aktivneVrcholy[i]]['stav']]['$'][j] , '$')
                }
            }
        }
        for (var i = 0; i < this.aktivneVrcholy.length; i++) {
            prechody.push(this.prechody[this.vrcholy[this.aktivneVrcholy[i]]['stav']]);
            idPrechodov.push(this.aktivneVrcholy[i])
        }
        this.resetuj();
        for (var i = 0; i < Object(prechody).length; i++) {
            for (var stav in prechody[i][this.slovo[this.iterator]]){
               this.pridajVrchol(idPrechodov[i], prechody[i][this.slovo[this.iterator]][stav], this.slovo[this.iterator]);
            }
        }
    },
    postavCestu: function (i) {
        var cesta = '';
        while ( this.vrcholy[i]['otec'] > -1) {
            cesta += this.vrcholy[i]['stav'];
            i = this.vrcholy[i]['otec'];
        }
        cesta += this.startovaciStav;
        return cesta.split('').reverse().join('')
    }
};