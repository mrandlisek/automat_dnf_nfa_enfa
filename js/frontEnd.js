document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.sidenav');
});
$(document).ready(function () {
    $('.sidenav').sidenav();
});
$('.chips').chips();
$('.states').chips({
    placeholder: 'Zadaj názvy stavov',
    secondaryPlaceholder: 'Pridaj stav',
    onChipAdd: function () {
        addSelect(M.Chips.getInstance($('.states')).chipsData);
        raiseTable();
    },
    onChipDelete: function () {
        addSelect(M.Chips.getInstance($('.states')).chipsData)
        raiseTable();
    }
});
$('.alphabet').chips({
    placeholder: 'Zadaj vstupnú abecedu',
    secondaryPlaceholder: 'Pridaj písmeno abecedy',
    onChipAdd: function () {
        raiseTable();
    },
    onChipDelete: function () {
        raiseTable();
    }
});
$(document).ready(function () {
    $('select').formSelect();
});

function addSelect(values) {
    var selects = $('.selectsState');
    $('.selectsState').empty();
    selects.innerHTML = '';
    for (var i = 0; i < values.length; i++) {
        selects.append($("<option></option>").attr("value", values[i].tag).text(values[i].tag));
        M.FormSelect.init(selects, {});
    }
}

function raiseTable() {
    $('.raiseTable').empty();
    $('.raiseTable').append('<table class="responsive-table raise"></table>');
    var table = $('.raise');
    var alphabet = M.Chips.getInstance($('.alphabet')).chipsData;
    var states = M.Chips.getInstance($('.states')).chipsData;
    table.append('<thead><tr>');
    table.append('<th> </th>');
    for (i = 0; i < alphabet.length; i++) {
        table.append('<th>' + alphabet[i].tag + '</th>');
    }
    table.append('<th>$</th>');
    table.append('</tr></thead>');

    for (i = 0; i < states.length; i++) {
        table.append('<tbody><tr>');
        table.append('<td>' + states[i].tag + '</td>');
        for (j = 0; j < alphabet.length + 1; j++) {
            table.append('' +
                '<td><div class="input-field">\n' +
                '  <select multiple class="selects raiseSelect">\n' +
                '  </select>\n' +
                '  <label>Prechod</label>\n' +
                '  </div></td>');
            table.append('</tr></tbody>');
        }
    }
    $('.raiseTable').append('<p><strong>Znak $</strong> znamená ε a nemusí byť definovaný pokiaľ sa jedná o DFA alebo NFA </p>');
    raiseSelect()
}

function raiseSelect() {
    var raiseSelect = $('.raiseSelect');
    $('.raiseSelect').empty();
    raiseSelect.innerHTML = '';
    var states = M.Chips.getInstance($('.states')).chipsData;
    for (var i = 0; i < states.length; i++) {
        $('.raiseSelect').append($("<option></option>").attr("value", states[i].tag).text(states[i].tag));
    }
    M.FormSelect.init(raiseSelect, {});
}

function processForm(form) {
    var x = form;
    var error;
    reintialAutomat();
    if (x[0].value) {
        window.Automat.slovo = x[0].value;
    } else {
        alert('Zadajte prosím vstupné slovo');
        error = true;
    }
    var states = M.Chips.getInstance($('.states')).chipsData;
    var alphabet = M.Chips.getInstance($('.alphabet')).chipsData;
    // for (var i = 0; i < window.Automat.slovo.length; i++) {
    //     if (!alphabet.includes(window.Automat.slovo[i])) {
    //         alert('Slovo nemôže byť rozpoznané lebo nemá definovanú rovnakú abecedu');
    //         error = true;
    //     }
    // }
    if (states.length === 0) {
        alert('Automat nemá definované stavy');
        error = true;
    }
    if (alphabet.length === 0) {
        alert('Automat nemá definovanú vstupnú abecedu');
        error = true;
    }
    for (var i = 0; i < states.length; i++) {
        window.Automat.stavy.push((states[i].tag))
    }

    if (M.FormSelect.getInstance($('.startState')).getSelectedValues().length > 1) {
        alert('Nie je povolené mať viac štartovacích stavov');
        error = true;
    } else if (M.FormSelect.getInstance($('.startState')).getSelectedValues().length === 0) {
        alert("Automat musí mať začiatočný stav");
        error = true;
    } else {
        window.Automat.startovaciStav = M.FormSelect.getInstance($('.startState')).getSelectedValues()[0];
    }

    if (M.FormSelect.getInstance($('.endState')).getSelectedValues().length === 0) {
        alert("Automat musí mať aspoň jeden koncový stav");
        error = true;
    } else {
        window.Automat.koncoveStavy = M.FormSelect.getInstance($('.endState')).getSelectedValues();
    }

    var row = 0, rowData = {}, rowOfTable = 0;
    var automat = {};
    for (var i = 0; i < $('.raiseSelect').length + 1; i++) {
        if (row < (alphabet.length + 1)) {
            if (row === alphabet.length) {
                if (!M.FormSelect.getInstance($('.raiseSelect')[i]).getSelectedValues().length == 0)
                    Object.assign(rowData, {'$': M.FormSelect.getInstance($('.raiseSelect')[i]).getSelectedValues()});
                row = row + 1;
            } else {
                var a = {};
                a[alphabet[row].tag] = M.FormSelect.getInstance($('.raiseSelect')[i]).getSelectedValues();
                if (!M.FormSelect.getInstance($('.raiseSelect')[i]).getSelectedValues().length == 0)
                    Object.assign(rowData, a);
                row = row + 1;
            }

        } else {
            row = 1;
            automat[window.Automat.stavy[rowOfTable]] = rowData;
            rowOfTable++;
            rowData = {};
            if (i < $('.raiseSelect').length) {
                var a = {};
                a[alphabet[0].tag] = M.FormSelect.getInstance($('.raiseSelect')[i]).getSelectedValues();
                if (!M.FormSelect.getInstance($('.raiseSelect')[i]).getSelectedValues().length == 0)
                    Object.assign(rowData, a);
            }
        }
    }

    window.Automat.prechody = automat;
    if (!error) {
        var result = [];
        console.log(window.Automat.slovo);
        result = window.Automat.init();
        printResult(result);
    }
}

function reintialAutomat() {
    window.Automat.stavy = [];
    window.Automat.koncoveStavy = [];
    window.Automat.slovo = '';
    window.Automat.startovaciStav = '';
    window.Automat.prechody = {};
    window.Automat.vrcholy = [];
    window.Automat.aktivneVrcholy = [0];
    window.Automat.aktivneStavy = [];
}

function printResult(result) {
    // if (result === 'gg wp') {
    //     alert('Automat nemá spravne definovanú prechodovú tabuľku');
    // } else {
    //     console.log(result);
    //     $(document.body).append('<div id="result" class="modal modal-fixed-footer">\n' +
    //         '    <div class="modal-content">\n' +
    //         '        <h4>' + result[0] + '</h4>\n' +
    //         '        <p><strong>' + result[1] + '</strong></p>\n' +
    //         '    </div>\n' +
    //         '    <div class="modal-footer">\n' +
    //         '        <a href="#" class="modal-close waves-effect waves-green btn-flat">Zavrieť</a>\n' +
    //         '    </div>\n' +
    //         '</div>');
    //     $('#result').modal();
    //     $('#result').modal('open');
    alert(result[0] + result[1]);
        result = [];
    // }
}