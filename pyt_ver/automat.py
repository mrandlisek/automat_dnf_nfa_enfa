
class StromOdvodeni:
    vrcholy = []
    aktivne_vrcholy = [0]
    aktivne_stavy = []

    def vypis_vrcholy(self):
        print('pocet vrcholov v strome: {}'.format(len(self.vrcholy)))

    def reset(self):
        self.aktivne_stavy = []
        self.aktivne_vrcholy = []

    def pridaj_vrchol(self, otec, stav, znak):
        if not stav in self.aktivne_stavy:
            self.vrcholy.append({
                'otec': otec,
                'stav': stav,
                'znak': znak,
            })
            self.aktivne_vrcholy.append(len(self.vrcholy)-1)
            self.aktivne_stavy.append(stav)

    def __init__(self, stav):
        self.vrcholy.append({
            'otec': -1,
            'stav': stav,
            'znak': '$',
        })
        self.aktivne_stavy.append(stav)


class Automat:
    slovo = list('aaabba')  # a*bb*a*
    stavy = ['1', '2', '3', '4']
    koncove_stavy = ['2', '3']
    prechody_v2 = {
        '1': {'a': ['1'], 'b': ['2', '3'], '$': ['3']},
        '2': {'a': ['3'], 'b': ['2']},
        '3': {'a': ['3'], 'b': ['4']},
        '4': {'a': ['4'], 'b': ['4']},
    }
    iterator = 0
    strom_odvodeni = StromOdvodeni('1')

    def __init__(self):
        for self.iterator in range(len(self.slovo)):
            self.get_prechody_v2()

        temp = True
        for i in self.strom_odvodeni.aktivne_vrcholy:
            if self.strom_odvodeni.vrcholy[i]['stav'] in self.koncove_stavy:
                self.postav_cestu(i)
                temp = False
        if temp:
            print('gg wp')
        self.strom_odvodeni.vypis_vrcholy()

    def get_prechody_v2(self):
        prechody = []
        id_prechodov = []
        for i in range(len(self.strom_odvodeni.aktivne_vrcholy)):
            if '$' in self.prechody_v2[self.strom_odvodeni.vrcholy[self.strom_odvodeni.aktivne_vrcholy[i]]['stav']]:
                for stav in self.prechody_v2[self.strom_odvodeni.vrcholy[self.strom_odvodeni.aktivne_vrcholy[i]]['stav']]['$']:
                    self.strom_odvodeni.pridaj_vrchol(self.strom_odvodeni.aktivne_vrcholy[i], stav, '$')

        for i in range(len(self.strom_odvodeni.aktivne_vrcholy)):
            prechody.append(self.prechody_v2[self.strom_odvodeni.vrcholy[self.strom_odvodeni.aktivne_vrcholy[i]]['stav']])
            id_prechodov.append(self.strom_odvodeni.aktivne_vrcholy[i])
        print(prechody)
        self.strom_odvodeni.reset()
        for i, prechod in enumerate(prechody):
            for stav in prechod[self.slovo[self.iterator]]:
                self.strom_odvodeni.pridaj_vrchol(id_prechodov[i], stav, self.slovo[self.iterator])

    def postav_cestu(self, i):
        cesta = ''
        while self.strom_odvodeni.vrcholy[i]['otec'] > -1:
            cesta += self.strom_odvodeni.vrcholy[i]['stav']
            i = self.strom_odvodeni.vrcholy[i]['otec']
        print(cesta[::-1])

Automat()
