import argparse

REGISTRE = [0] * 51  # 50 registrov nulty sa nepouziva
DEBUG = False
INST = False
INST_V = False

class Stroj:
    """
cita instrukcie
    inc <register>
    cpy <register> <register>
    zero <register>
    jmp <register> <register> <riadok programu>

inicializuje funkcie
    fnc <umiestnenie suboru> [registre vstupov] <register vystup>
    automaticky vycisti pozadovane registre a vlozi vstupy do prvych registrov v danom poradi
    na konci vlozi hodnotu prveho registra do pozadovaneho

"nulty" riadok suboru
    init <pocet pouzivanych registrov> <pozadovany pocet vstupov>

spustenie python URM.py <vstupny subor> [vstupy] /-d pre debug
spustenie python URM.py <vstupny subor> [vstupy] /-i pre vypis instrunkcie a stavu registra po jej vykonani
spustenie python URM.py <vstupny subor> [vstupy] /-iv pre vypis instukcie a stavu registra len hlavneho programu nie jeho podfunkcii
    """

    citana_instrukcia = 1

    def __init__(self, filename, M=None, inputs=None, verb=False):
        self.verb = verb
        if filename:
            with open(filename, 'r') as file:
                self.instrukcie = file.read()

        self.instrukcie = list(self.instrukcie.split('\n'))
        if 'init' in self.instrukcie[0]:
            init = [int(s) for s in self.instrukcie[0].replace(',', ' ').split() if s.isdigit()]
            if len(init) == 1:
                numbers = []
            elif M and len(M) == init[1]:
                numbers = [REGISTRE[i] for i in M]
            elif inputs and len(inputs) == init[1]:
                numbers = inputs
            else:
                raise('syntax error (zle vstupy na inite)')
            self.init(init[0], numbers)
        else:
            raise('syntax error (nezadefinovany init)')

        if DEBUG:
            print(filename, [REGISTRE[i] for i in M] if M else inputs)
            print("start "+filename, REGISTRE[1:])
        while self.citana_instrukcia < len(self.instrukcie):
            self.citanie()

        if self.verb:
            if DEBUG:
                print("end " + filename, REGISTRE[1:])
            print(REGISTRE[1])

    def init(self, A, M):
        for i in range(1, A+1):
            REGISTRE[i] = 0
        for i, num in enumerate(M):
            REGISTRE[i+1] = num

    def inc(self, X):
        REGISTRE[X] += 1
        self.citana_instrukcia += 1

    def cpy(self, Y, Z):
        REGISTRE[Z] = REGISTRE[Y]
        self.citana_instrukcia += 1

    def zero(self, Q):
        REGISTRE[Q] = 0
        self.citana_instrukcia += 1

    def jmp(self, A, B, C):
        if REGISTRE[A] == REGISTRE[B]:
            self.citana_instrukcia = C
        else:
            self.citana_instrukcia += 1

    def call_func(self, filename, M, A):  # matica vstupov, A vystup
        Stroj(filename=filename, M=M)
        REGISTRE[A] = REGISTRE[1]
        if DEBUG:
            print('end ' + filename, REGISTRE[1:])
        self.citana_instrukcia += 1

    def verbose(self, string):
        if INST or (INST_V and self.verb):
            self.verbose_str = string

    def citanie(self):
        if self.citana_instrukcia >= len(self.instrukcie):
            return
        instrukcia = self.instrukcie[self.citana_instrukcia]
        numbers = [int(s) for s in instrukcia.replace(',', ' ').split() if s.isdigit()]
        if 'inc' in instrukcia.split()[0]:
            self.verbose('{}: inc {}'.format(self.citana_instrukcia, numbers[0]))
            self.inc(numbers[0])
        elif 'cpy' in instrukcia.split()[0]:
            self.verbose('{}: cpy {} {}'.format(self.citana_instrukcia, numbers[0], numbers[1]))
            self.cpy(numbers[0], numbers[1])
        elif 'zero' in instrukcia.split()[0]:
            self.verbose('{}: zero {}'.format(self.citana_instrukcia, numbers[0]))
            self.zero(numbers[0])
        elif 'jmp' in instrukcia.split()[0]:
            self.verbose("{}: jmp {} {} #{}".format(self.citana_instrukcia, numbers[0], numbers[1], numbers[2]))
            self.jmp(numbers[0], numbers[1], numbers[2])
        elif 'fnc' in instrukcia.split()[0]:
            num = numbers.pop(len(numbers)-1)
            filename = instrukcia.split()[1]
            self.verbose('{}: fnc {} {} {}'.format(self.citana_instrukcia, filename, numbers, num))
            self.call_func(filename, numbers, num)
        else:
            raise('syntax error')
        if INST or (INST_V and self.verb):
            print(self.verbose_str, REGISTRE[1:])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Zadajte nazov suboru a vstupy')
    parser.add_argument('string', help='vstupny subor a vstupy', nargs='+')
    parser.add_argument('-d', action='store_true', help='DEBUG')
    parser.add_argument('-i', action='store_true', help='Instrukcie')
    parser.add_argument('-iv', action='store_true', help='Instrukcie len hlavneho stroje')

    args = parser.parse_args()
    arg_list = args.string
    filename = arg_list.pop(0)
    DEBUG = args.d
    INST = args.i
    INST_V = args.iv
    Stroj(filename=filename, inputs=[int(s) for s in arg_list], verb=True)
